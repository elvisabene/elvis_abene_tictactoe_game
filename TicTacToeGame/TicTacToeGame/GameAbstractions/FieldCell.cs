﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TicTacToeGame.GameAbstractions
{
    public class FieldCell
    {
        private readonly int _x;
        private readonly int _y;
        public char Symbol { get; set; }

        public bool IsEmpty
        {
            get
            {
                return Symbol == default(char);
            }
        }

        public FieldCell(int X, int Y)
        {
            if (X == 0)
            {
                _x = 2;
            }
            else
            {
                _x = 2 + X * 4;
            }

            if (Y == 0)
            {
                _y = 1;
            }
            else
            {
                _y = Y * 2 + 1;
            }
        }

        public void SetCursor()
        {
            Console.SetCursorPosition(_x, _y);
        }

        public void SetCharInCell(char character)
        {
            Symbol = character;
            SetCursor();
            Console.Write(character);
        }
    }
}
