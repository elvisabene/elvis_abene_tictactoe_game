﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace TicTacToeGame.GameAbstractions
{
    public static class Field
    {
        public static int Rank { get; private set; }
        private const char FrameChar = '*';
        private static FieldCell[][] _cells;
        public static int AfterFieldLine { get; set; }

        public static void CreateField(int n)
        {
            Console.Clear();
            Rank = n;
            AfterFieldLine = 2 * n + 2;
            DrawField();
            MakeCells();
            SetPosition(0, 0);
        }

        public static void SetStartPosition(ref int x, ref int y)
        {
            x = -1;
            y = 0;
            GetNextEmptyCell(ref x, ref y);
            SetPosition(x, y);
        }

        public static void SetPosition(int x, int y)
        {
            if (x > Rank - 1 || x < 0
               || y > Rank - 1 || y < 0)
            {
                return;
            }
            _cells[y][x].SetCursor();
        }

        private static void DrawField()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            int border = 2 * Rank + 1;
            for (int y = 0; y < border; y++)
            {
                for (int x = 0; x < border; x++)
                {
                    if (y % 2 == 0)
                    {
                        Console.Write($"{FrameChar} ");
                        continue;
                    }
                    if (x % 2 == 0)
                    {
                        Console.Write($"{FrameChar} ");
                        continue;
                    }
                    Console.Write("  ");
                }
                Console.WriteLine();
            }
            Console.ResetColor();
        }

        private static void MakeCells()
        {
            _cells = new FieldCell[Rank][];
            for (int i = 0; i < Rank; i++)
            {
                _cells[i] = new FieldCell[Rank];
            }

            for (int y = 0; y < Rank; y++)
            {
                for (int x = 0; x < Rank; x++)
                {
                    _cells[y][x] = new FieldCell(x, y);
                }
            }
        }

        public static void SetChar(char symbol, int x, int y)
        {
            if (x > Rank - 1 || x < 0
               || y > Rank - 1 || y < 0)
            {
                return;
            }
            _cells[y][x].SetCharInCell(symbol);
        }

        public static bool OverBorder(int k)
        {
            if (k >= Rank || k < 0)
            {
                return true;
            }

            return false;
        }

        public static bool CellIsEmpty(int x, int y)
        {
            return _cells[y][x].IsEmpty;
        }

        public static void GetNextEmptyCell(ref int x, ref int y)
        {
            for (int i = y; i < _cells.Length; i++)
            {
                for (int j = i == y ? x + 1 : 0; j < _cells[i].Length; j++)
                {
                    if (_cells[i][j].IsEmpty)
                    {
                        x = j;
                        y = i;
                        return;
                    }
                }
            }
        }

        public static void GetPreviousEmptyCell(ref int x, ref int y)
        {
            for (int i = y; i > -1; i--)
            {
                for (int j = i == y ? x - 1 : _cells[i].Length - 1; j > -1; j--)
                {
                    if (_cells[i][j].IsEmpty)
                    {
                        x = j;
                        y = i;
                        return;
                    }
                }
            }
        }

        public static void GetEmptyCellBelow(ref int x, ref int y)
        {
            for (int i = y + 1; i < _cells.Length; i++)
            {
                if (_cells[i][x].IsEmpty)
                {
                    y = i;
                    return;
                }
            }
        }

        public static void GetEmptyCellAbove(ref int x, ref int y)
        {
            for (int j = y - 1; j > -1; j--)
            {
                if (_cells[j][x].IsEmpty)
                {
                    y = j;
                    return;
                }
            }
        }

        public static void GetFirstEmptyCell(ref int x, ref int y)
        {
            for (int i = 0; i < _cells.Length; i++)
            {
                for (int j = 0; j < _cells[i].Length; j++)
                {
                    if (_cells[i][j].IsEmpty)
                    {
                        x = j;
                        y = i;
                    }
                }
            }
        }

        public static void GetRandomEmptyCell(ref int x, ref int y)
        {
            Random random = new Random();
            for (int k = 0; k < 20; k++)
            {
                int i = random.Next(0, Rank);
                int j = random.Next(0, Rank);
                if (_cells[i][j].IsEmpty)
                {
                    SetPosition(x, y);
                    y = i;
                    x = j;
                }


                Thread.Sleep(20);
            }
        }

        private static int GetHorizontalCombinations(char symbol)
        {
            int horComb = 0;
            int charInComb = 0;
            for (int i = 0; i < Rank; i++)
            {
                for (int j = 0; j < Rank; j++)
                {
                    if (_cells[i][j].Symbol == symbol)
                    {
                        charInComb++;
                        if (charInComb >= 3)
                        {
                            horComb++;
                        }
                        continue;
                    }
                    charInComb = 0;
                }
                charInComb = 0;
            }

            return horComb;
        }

        private static int GetVerticalCombinations(char symbol)
        {
            int vertComb = 0;
            int charInComb = 0;
            for (int i = 0; i < Rank; i++)
            {
                for (int j = 0; j < Rank; j++)
                {
                    if (_cells[j][i].Symbol == symbol)
                    {
                        charInComb++;
                        if (charInComb >= 3)
                        {
                            vertComb++;
                        }
                        continue;
                    }
                    charInComb = 0;
                }
                charInComb = 0;
            }

            return vertComb;
        }

        private static int GetDiagonalCombinations(char symbol)
        {
            var checkedRightCells = new List<FieldCell>(Rank);
            int diagComb = 0;
            int charInComb = 0;
            for (int i = 0; i < Rank; i++)
            {
                for (int j = 0; j < Rank; j++)
                {
                    charInComb = 0;
                    int ik = i;
                    for (int jk = j; jk < Rank; jk++)
                    {
                        if (_cells[ik][jk].Symbol == symbol)
                        {
                            if (checkedRightCells.Contains(_cells[ik][jk]))
                            {
                                break;
                            }
                            else
                            {
                                checkedRightCells.Add(_cells[ik][jk]);
                            }

                            charInComb++;
                            if (charInComb >= 3)
                            {
                                diagComb++;
                            }

                            ik++;
                            if (ik == Rank)
                            {
                                break;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }

            var checkedLeftCells = new List<FieldCell>(Rank);
            for (int i = 0; i < Rank; i++)
            {
                for (int j = 0; j < Rank; j++)
                {
                    charInComb = 0;
                    int ik = i;
                    for (int jk = j; jk > -1; jk--)
                    {
                        if (_cells[ik][jk].Symbol == symbol)
                        {
                            if (checkedLeftCells.Contains(_cells[ik][jk]))
                            {
                                break;
                            }
                            else
                            {
                                checkedLeftCells.Add(_cells[ik][jk]);
                            }
                            charInComb++;

                            if (charInComb >= 3)
                            {
                                diagComb++;
                            }

                            ik++;
                            if (ik == Rank)
                            {
                                break;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }

            return diagComb;
        }

        public static int GetCombinationsCount(char symbol)
        {
            int horComb = GetHorizontalCombinations(symbol);
            int vertComb = GetVerticalCombinations(symbol);
            int diagComb = GetDiagonalCombinations(symbol);
            return horComb + vertComb + diagComb;
        }

        public static bool HasEmptyCells()
        {
            for (int i = 0; i < _cells.Length; i++)
            {
                for (int j = 0; j < _cells[i].Length; j++)
                {
                    if (_cells[i][j].IsEmpty)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
