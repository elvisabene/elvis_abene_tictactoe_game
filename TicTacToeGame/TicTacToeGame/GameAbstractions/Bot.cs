﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TicTacToeGame.GameAbstractions
{
    public class Bot : Player
    {
        public Bot(char symbol, ConsoleColor color) : base(symbol, color)
        {
        }

        public override void MakeMove()
        {
            Field.SetStartPosition(ref _x, ref _y);
            Field.GetRandomEmptyCell(ref _x, ref _y);
            Console.ForegroundColor = _color;
            Field.SetChar(_symbol, _x, _y);
            Console.ResetColor();
            CombinationsCount = Field.GetCombinationsCount(_symbol);
        }
    }
}
