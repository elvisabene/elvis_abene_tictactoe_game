﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TicTacToeGame.GameAbstractions
{
    public class Symbol
    {
        public char Character { get; }

        public Symbol(char character)
        {
            Character = character;
        }
    }
}
