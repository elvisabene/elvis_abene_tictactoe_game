﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TicTacToeGame.GameAbstractions
{
    public class Player
    {
        protected readonly char _symbol;
        protected int _x;
        protected int _y;
        protected ConsoleColor _color;
        public int CombinationsCount { get; set; }

        public Player(char symbol, ConsoleColor color)
        {
            _symbol = symbol;
            _color = color;
        }

        public virtual void MakeMove()
        {
            Field.SetStartPosition(ref _x, ref _y);
            while (true)
            { 
                switch (Console.ReadKey().Key)
                {
                    case ConsoleKey.RightArrow:
                        if (!Field.OverBorder(_x + 1)
                            && Field.CellIsEmpty(_x + 1, _y))
                        {
                            _x++;
                        }
                        else
                        {
                            Field.GetNextEmptyCell(ref _x, ref _y);
                        }
                        break;
                    case ConsoleKey.LeftArrow:
                        if (!Field.OverBorder(_x - 1)
                            && Field.CellIsEmpty(_x - 1, _y))
                        {
                            _x--;
                        }
                        else
                        {
                            Field.GetPreviousEmptyCell(ref _x, ref _y);
                        }
                        break;
                    case ConsoleKey.UpArrow:
                        if (!Field.OverBorder(_y - 1))
                        {
                            if (Field.CellIsEmpty(_x, _y - 1))
                            {
                                _y--;
                            }
                            else
                            {
                                Field.GetEmptyCellAbove(ref _x, ref _y);
                            }
                        }
                        break;
                    case ConsoleKey.DownArrow:
                        if (!Field.OverBorder(_y + 1))
                        {
                            if (Field.CellIsEmpty(_x, _y + 1))
                            {
                                _y++;
                            }
                            else
                            {
                                Field.GetEmptyCellBelow(ref _x, ref _y);
                            }
                        }
                        break;
                    case ConsoleKey.Enter:
                        Console.ForegroundColor = _color;
                        Field.SetChar(_symbol, _x, _y);
                        Console.ResetColor();
                        CombinationsCount = Field.GetCombinationsCount(_symbol);
                        return;
                }
                Field.SetPosition(_x, _y);
            }
        }
    }
}
