﻿using System;
using System.Collections.Generic;
using System.Text;
using TicTacToeGame.Menu.GameAbstractions;

namespace TicTacToeGame.GameAbstractions
{
    public static class Game
    {
        private static Player _player1;
        private static Player _player2;
        public static void Start()
        {
            SetPlayers();
            int n = GetFieldRank();
            Field.CreateField(n);
            while (true)
            {
                if (GameIsOver())
                {
                    break;
                }
                _player1.MakeMove();
                if (GameIsOver())
                {
                    break;
                }
                _player2.MakeMove();
            }

            PrintResults();
        }

        private static int GetFieldRank()
        {
            Console.WriteLine("Введите размерность поля: ");
            int n = 0;
            while (true)
            {
                if (!int.TryParse(Console.ReadLine(), out n) || n < 3)
                {
                    Console.WriteLine("Введите целое число от 3.");
                    continue;
                }
                break;
            }

            return n;
        }

        private static string[][] GetMenuOptions()
        {
            var menu = new string[3][];
            menu[0] = new string[]
            {
                "1. Одиночная игра (против бота).",
                "2. Игра на двоих.",
            };
            menu[1] = new string[]
            {
                "1. Первым ходит бот - крестиком (X).",
                "2. Первым ходит бот - ноликом (0).",
                "3. Первым ходит игрок - крестиком (X).",
                "4. Первым ходит игрок - ноликом (0).",
            };
            menu[2] = new string[]
            {
                "1. Первый игрок ходит крестиком (X).",
                "2. Первый игрок ходит ноликом (0).",
            };

            return menu;
        }

        private static void SetPlayers()
        {
            ConsoleMenu firstMenu = new ConsoleMenu(GetMenuOptions()[0]);
            int modeOption = firstMenu.GetOption();

            if (modeOption == 0)
            {
                ConsoleMenu secondMenu = new ConsoleMenu(GetMenuOptions()[1]);
                int charOption = secondMenu.GetOption();
                switch (charOption)
                {
                    case 0:
                        _player1 = new Bot('X', ConsoleColor.Green);
                        _player2 = new Player('0', ConsoleColor.Blue);
                        break;
                    case 1:
                        _player1 = new Bot('0', ConsoleColor.Green);
                        _player2 = new Player('X', ConsoleColor.Blue);
                        break;
                    case 2:
                        _player1 = new Player('X', ConsoleColor.Green);
                        _player2 = new Bot('0', ConsoleColor.Blue);
                        break;
                    case 3:
                        _player1 = new Player('0', ConsoleColor.Green);
                        _player2 = new Bot('X', ConsoleColor.Blue);
                        break;
                }
            }
            else
            {
                ConsoleMenu secondMenu = new ConsoleMenu(GetMenuOptions()[2]);
                int charOption = secondMenu.GetOption();
                switch (charOption)
                {
                    case 0:
                        _player1 = new Player('X', ConsoleColor.Green);
                        _player2 = new Player('0', ConsoleColor.Blue);
                        break;
                    case 1:
                        _player1 = new Player('0', ConsoleColor.Green);
                        _player2 = new Player('X', ConsoleColor.Blue);
                        break;
                }
            }
        }

        private static bool GameIsOver()
        {
            int separation = Math.Abs(_player1.CombinationsCount - _player2.CombinationsCount);
            if (separation >= (Field.Rank - 3) * 2 + 1)
            {
                return true;
            }
            return !Field.HasEmptyCells();
        }

        private static void PrintResults()
        {
            int firstPlayerScore = _player1.CombinationsCount;
            int secondPlayerScore = _player2.CombinationsCount;
            string message = "Игра закончена, результат: ";
            if (firstPlayerScore > secondPlayerScore)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                message += "первый игрок победил!";
            }
            else if (firstPlayerScore < secondPlayerScore)
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                message += "второй игрок победил!";
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                message += "ничья!";
            }

            Console.SetCursorPosition(0, Field.AfterFieldLine);
            Console.WriteLine(message);
            Console.ResetColor();
        }
    }
}
