﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TicTacToeGame.Menu.GameAbstractions
{
    public class ConsoleMenu
    {
        private readonly string[] _menuOptions;
        private int _currentOption;

        public ConsoleMenu(string[] menuOptions)
        {
            _menuOptions = menuOptions;
            _currentOption = 0;
        }

        public int GetOption()
        {
            SetCursorOnOption();
            while (true)
            {
                switch (Console.ReadKey().Key)
                {
                    case ConsoleKey.UpArrow:
                        if (_currentOption == 0)
                        {
                            break;
                        }
                        _currentOption--;
                        break;
                    case ConsoleKey.DownArrow:
                        if (_currentOption == _menuOptions.Length - 1)
                        {
                            break;
                        }
                        _currentOption++;
                        break;
                    case ConsoleKey.Enter:
                        Console.Clear();
                        return _currentOption;
                }
                SetCursorOnOption();
            }
        }

        private void SetCursorOnOption()
        {
            Console.Clear();
            Console.SetCursorPosition(0, 0);
            for (int i = 0; i < _menuOptions.Length; i++)
            {
                if (i == _currentOption)
                {
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.WriteLine(_menuOptions[_currentOption]);
                    Console.ResetColor();
                    continue;
                }
                Console.WriteLine(_menuOptions[i]);
            }
        }
    }
}
