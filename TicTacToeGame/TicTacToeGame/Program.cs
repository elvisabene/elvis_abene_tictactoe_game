﻿using System;
using TicTacToeGame.GameAbstractions;

namespace TicTacToeGame
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            Game.Start();
            Console.ReadKey();
        }
    }
}
